from Crypto.Cipher import AES
from Crypto import Random
from Crypto.Util import Counter
from base64 import b64encode

key = b'sixteen byte key'
e1 = ""
e2 = ""

def encrypting(t):
	cipher = AES.new(key=key, mode=AES.MODE_CTR, counter=Counter.new(AES.block_size * 8))
	e1 = cipher.encrypt(t)
	return e1

def decrypting(e1):
	cipher = AES.new(key=key, mode=AES.MODE_CTR, counter=Counter.new(AES.block_size * 8))
	print(Counter.new(AES.block_size * 8))
	plaintext = cipher.decrypt(e1)#(b'#7g\xc2\xb9\xb8')
	print(plaintext)
	
e1 = encrypting(b"Torau!")
decrypting(e1)	

e1 = encrypting(b"Test")
decrypting(e1)	