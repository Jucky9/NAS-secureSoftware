import socket, ssl, secrets, errno
from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES
from Crypto import Random
from Crypto.Util import Counter
from base64 import b64encode, b64decode
from OpenSSL import crypto
import csv
import os
from shutil import copytree


class Client:
	def __init__(self,addr = '127.0.0.1', port = 1024):
		self._addr = addr
		self._port = port
		self._aesKey = secrets.token_bytes(16)
		self._certificatePath = "certificate.pem"
		self._macrosDictionnary = {}
		self.listePathsRecursifs = []
		self._certificate = crypto.load_certificate(crypto.FILETYPE_PEM, open(self._certificatePath,"r").read())
		context = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
		context.load_verify_locations('certificate.pem')
		socketCreated = False 
		try : 
			socketC = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
			self._ssocket = context.wrap_socket(socketC,server_hostname='SECU')
			socketCreated = True

		except _ssocket.error :
			print("could not create a socket")
		if(socketCreated):
			try:
				refused = False
				self._ssocket.connect((self._addr,self._port))
			except:
				print("Couldn't connect with the server")
				return None
			try:
				self._ssocket.send("1".encode())
				self._ssocket.send("1".encode())
			except:
				print("There's probably already someone connected with the same adress...")
				refused=True
			
			if(not refused):
				self.getMacros()
				self.secureConnection()
				self.userAction()

	def userAction(self):
		self.userConnect()

	def userConnect(self):
		userChoiceVerif = False
		while not userChoiceVerif:
			userChoice = self.howToConnect()
			if(userChoice=="1"):
				self.signUp()
			elif(userChoice=="2"):
				self.logIn()
			elif(userChoice=="3"):
				self.quit()
				userChoiceVerif=True
					

	def getMacros(self):
		with open('macrosFile.txt') as f:
			reader = csv.DictReader(f, delimiter = ':')
			for row in reader:
				self._macrosDictionnary[row['Code']] = row['Action']

	def getMacroValue(self, value):
			for code, action in self._macrosDictionnary.items():
				if action == value:
					return code		

	def howToConnect(self) :
		os.system("clear")
		print("Hello and welcome ! ")
		print("Would you like to sign up (1) or log in (2) or quit the program (3) ?")
		userChoiceVerif = False
		userChoice = ""
		while not userChoiceVerif :
			try:
				userChoice = input()
				if(userChoice=="1" or userChoice=="2" or userChoice=="3"):
					userChoiceVerif=True
				else:
					print("Wrong choice, type 1 to sign up or 2 to log in.")
			except KeyboardInterrupt:
				userChoiceVerif=True
				userChoice="3"
		return userChoice
		
		

	def signUp(self):
		print("you are signing up!")
		userOK = False
		while not userOK :
			try:
				self.send(self.getMacroValue("SIGNUP"))
				username = input("Username : ")
				isPasswordOk = False
				while not isPasswordOk : 
					password = input("Password : ")
					verifPassword = input("Type your password again : ")
					if(password==verifPassword and password!=" "):
						isPasswordOk = True
					else :
						print("Didn't type the same password")
				print("To have more security, could you answer the following question( remember your answer) : ")
				userAnswerLife = input("What does life mean? ")
				self.send(username)
				print("Sign up---sent username")
				serverAnswer = self.receive()
				if(serverAnswer=="OK"):
					userOK = True
				else:
					print("Username already used, please find another one")
				self.send(password)
				print("Sign up---sent password")
				self.send(userAnswerLife)
				print("Sign up---sent answerLife")
			except KeyboardInterrupt:
				userOK=True

	def logIn(self):
		print("you are logging in")
		userOk = False
		self.send(self.getMacroValue("LOGIN"))
		count = 0
		while not userOk:
			try:
				username = input("Insert your username : ")
				password = input("Insert your password : ")
				self.send(username)
				self.send(password)
				if(count>3):
					secretPass = input("What was your answer to 'What does life mean?' :  ")
					self.send(secretPass)
				count+=1
				if(self.receive()=="OK"):
					isAdmin = self.receive()
					if(isAdmin=="1"):
						self.logInMenu(True)#en paramètre on mettra le boolean qui dit si c'est un admin ou pas
					else:
						self.logInMenu()

					userOk = True
				else:
					print("Username or password wrong. It is also possible that you are not yet accepted by the administrators.")
			except KeyboardInterrupt:
				userOk=True

	def quit(self):
		print("Closing the program!")
		self._ssocket.close()

	def sendFile(self,fileName, whereIsStored, singleFile = False):
		self.send(fileName) # Nom du fichier
		print("sent file name: "+fileName)
		#self._ssocket.send('Current Directory') #Genre histoire de group ou quoi, ou ce trouve le fichier 
		#self.sendFile("/Test.txt",os.getcwd()) Commencer lancé la méthode. On a d'abord du coup le nom du fichier avec le "/" avant et après là ou ce situe le fichier chez le client
		if (not singleFile):
			file = whereIsStored+"/"+fileName
		else:
			file = whereIsStored
		with open(file, 'rb') as fileToSend:
			for data in fileToSend:
				print(data)
				self.send(data, True)
			fileToSend.close()
		self.send(' ')
		print("sent ' ' to end file transfer")


	def receiveFile(self, path, singleFile = False):
		print("Waiting file from the server")
		try:
			file = self.receive() 
			print("receive FILENAME======")
			receivingFile = True
			if (not singleFile):
				fullPath = "downloads"+"/"+path+file
			else:
				fullPath = path+"/"+file	
			with open(fullPath, 'wb') as fileToWrite:
				while receivingFile:
					data = self.receive(True)
					print(data)
					if(data==b' '):
						receivingFile = False
						fileToWrite.close()
						print("end transfer single file")
					else:
						fileToWrite.write(data)
		except socket.error as e:
			self.handleKeyboardInterrupt(e,False)


	def close(self):
		self._ssocket.close()

	def logInMenu(self, admin = False):
		userChoiceVerif = False
		while not userChoiceVerif:
			os.system("clear")
			print("You are logged in APPHOME!\n")
			print("Which operation would you like to perform?\n")
			print("1) Browse all files\n")
			print("2) Upload a file to the server\n")
			print("3) Delete a file from the server\n")
			print("4) Download a file from the server\n")
			print("5) Share a file\n")
			if (admin):
				print("6) validate new users\n")
			print("7) create empty folder")	
			print("8) Log out\n") 
			userChoice = input("Type your choice: ")
			if(userChoice=="1" ):#browse all files
				self.browseAllFiles()
			elif(userChoice=="2"):#upload a file
				self.uploadFiles()
			elif(userChoice=="3"):#delete a file
				self.deleteFiles()
			elif(userChoice=="4"):#download a file	
				self.downloadFiles()	
			elif(userChoice=="5"):#Group file
				self.groupFileMenu()
			elif(userChoice=="6" and admin):
				print("Validation menu for new users")	
				self.validateUsers()
			elif(userChoice=="7"):#Empty directory
				self.createEmptyDirectory()
			elif(userChoice=="8"):#Log out
				print("you are going to be disconnected.. goodbye!")
				self.logOut()
				userChoiceVerif=True	
			else:
				print("You have to type a number between 1-5\n")
				print("Please try again!\n")	
					
	def groupFileMenu(self):
		self.send(self.getMacroValue("GROUPFILE"))
		quit = False
		notFinished = False
		totalNum = 0
		message = ""
		while not notFinished:
			messageServer = self.receive()
			if (messageServer!="FINISHED"):
				message+= messageServer
				message+="\n"
			else:
				totalNum = self.receive()
				notFinished = True
		print("Which file do you want to share?")
		while not quit:
			os.system("clear")
			print(message)
			print("Which file do you want to share?")
			userChoice = input("Type your choice( Type q to quit) : ")
			if(userChoice>="0" and userChoice<=str(totalNum)):
				self.send(self.getMacroValue("OKTYPE"))
				fileToShare = self.send(str(userChoice))
				userShareWith = input("Type the username of the one you want to share your file with : ")
				permissionAllowed = input("What type of permission is the user allowed to do with the file?\n('r' is to read, 'w' is to read and modify)\n")
				isPermissionAccepted = False
				while(permissionAllowed!='r' and permissionAllowed!='w'):
					permissionAllowed = input("WRONG PERMISSION TYPED.\nWhat type of permission is the user allowed to do with the file?\n('r' is to read, 'w' is to read and modify)\n")
				wantGroupAgree = input("Do you allow the 'group agree' function? \nThe group agree function allowed the deletation of a file only if everyone agrees\n(Type 'y' for yes and 'n' for No)\n")
				isGroupAgreeValid = False
				while(wantGroupAgree!="y" and wantGroupAgree!="n"):
					wantGroupAgree = input("WRONG AGGREEMENT TYPED.\nDo you allow the 'group agree' function? \nThe group agree function allowed the deletation of a file only if everyone agrees\n(Type 'y' for yes and 'n' for No)\n")
				print("Thank you for you informations. The user,if well typed, will have the opportunity to access the file with the permissions you gave.")
				if(userShareWith!=''):
					self.send(userShareWith)
					isUsernameValid = self.receive()
					if(self._macrosDictionnary[isUsernameValid]=="USERNAMEOK"):
						self.send(permissionAllowed)
						self.send(wantGroupAgree)

			elif(userChoice=="q"):
				quit = True
				self.send(self.getMacroValue("QUIT"))
			else:
				print("You have to type a number between 0 and " + str(totalNum))
			input("Press Enter to continue ")


	def browseAllFiles(self):
		os.system("clear")
		print("This is the browseAllFiles Menu\n")
		self.send(self.getMacroValue("BROWSEFILES"))
		print("the files you have access are: \n")
		notFinished = False
		while not notFinished:
			message = self.receive()
			if (message!="FINISHED"):
				print(message+"\n")
			else:
				notFinished = True
		input("press enter to go back ")

	def sendDirectory(self, fileName, path):
		print("envoie d'un répertoire")
		print("nom du repertoire: "+fileName)
		self.send(fileName)
		listOfFiles = os.listdir(path)
		for file in listOfFiles:
			self.send("CONTINUE")
			print("SENT CONTINUE====")
			if (os.path.isdir(path+"/"+file)):
				self.send("DIRECTORY")
				print("sent DIRECTORY=======")
				self.send(self.getCurrentPath())
				print("sent CURRENTPATH=====")
				self.listePathsRecursifs.append(file)
				self.sendDirectory(file,path+"/"+file)
			else:
				self.send("NOTDIRECTORY")
				print("sent NOTDIRECTORY=======")
				self.send(self.getCurrentPath())
				print("sent CURRENTPATH=====")
				self.listePathsRecursifs.append(file)
				self.sendFile(file, path)

			self.listePathsRecursifs.pop()	

	def getCurrentPath(self):
		currentpath = ""
		for i in range(len(self.listePathsRecursifs)):
			if (i!=len(self.listePathsRecursifs)):
				currentpath+=self.listePathsRecursifs[i]+"/"
			else:
				currentpath+=self.listePathsRecursifs[i]	
		return currentpath		

	def receiveDirectory(self, path = ""):
		directoryToCreate = self.receive()
		try:
			os.mkdir("downloads"+"/"+path+directoryToCreate)
		except OSError as e:
			print("your directory already exists!")
		notFinished = False
		while (not notFinished):
			print("rentre encore dansle <hile de receiveDirectory()")
			finishOrNot = self.receive()
			if (finishOrNot=="CONTINUE"):
				response=self.receive()
				if (response=="DIRECTORY"):
					print("received DIRECTORY=======")
					newPath=self.receive()
					print("received currentpath======")
					return self.receiveDirectory(newPath)	
				elif(response=="NOTDIRECTORY"):
					print("received NOTDIRECTORY=======")
					newPath=self.receive()
					print("received currentpath======")
					self.receiveFile(newPath)
			elif (finishOrNot=="NOTCONTINUE"):
				notFinished = True
				print("receive NOT CONTINUE")
				return True	

	def uploadFiles(self):
		os.system("clear")
		print("Upload file Menu\n")	
		#try:
		correctPath = False
		while (not correctPath):
			fullpath = input("input the full-path to your file/directory: ")
			fileName = self.getFileName(fullpath)
			if (os.path.exists(fullpath)):
				correctPath = True
				self.send(self.getMacroValue("UPLOADFILE"))
				if (os.path.isdir(fullpath)):
					self.send("DIRECTORY")
					self.listePathsRecursifs.append(fileName)
					self.sendDirectory(fileName, fullpath)
					self.send("NOTCONTINUE")
					print("sent NOTCONTINUE=====")
				else:
					self.send("NOTDIRECTORY")
					self.sendFile(fileName, fullpath, True)
			else:
				print("this directory or file is not valid, type again")		
		
		#except :
			#print("this directory or file is not valid")	
	def createEmptyDirectory(self):
		os.system("clear")
		print("create directory Menu\n")
		self.send(self.getMacroValue("CREATEDIRECTORY"))
		print("the directories where you can create an empty directory are : \n")
		notFinished = False
		receiveFiles = []
		while not notFinished:
			message = self.receive()
			if (message!="FINISHED"):
				receiveFiles.append(message)
				print(message+"\n")
			else:
				notFinished = True
		correctFile = False		
		while not correctFile:		
			toCreate=input("type the path of the directory where you want to create:  ")
			if (toCreate in receiveFiles):
				correctFile = True
				directoryName=input("type the name of the directory you want to create:  ")
				self.send(toCreate)
				self.send(directoryName)

			else:
				print("\n You didin't type a correct file path, please type again!")	
			input("Enter to continue ")
			
	def deleteFiles(self):
		os.system("clear")
		print("DeleteFiles Menu\n")
		self.send(self.getMacroValue("DELETEFILE"))
		print("the files you can delete are : \n")
		notFinished = False
		receiveFiles = []
		while not notFinished:
			message = self.receive()
			if (message!="FINISHED"):
				receiveFiles.append(message)
				print(message+"\n")
			else:
				notFinished = True
		correctFile = False		
		while not correctFile:		
			toDelete=input("type the file with the path you want to delete:  ")
			if (toDelete in receiveFiles):
				correctFile = True
				self.send(toDelete)

			else:
				print("\n You didin't type a correct file path, please type again!")	
			input("Enter to continue ")
		
	def getFileName(self, path):
		listing = path.split("/")
		return listing[len(listing)-1]

	def downloadFiles(self):
		os.system("clear")
		print("DownloadFiles Menu\n")
		print(os.getcwd())
		try:
			os.mkdir("downloads")
		except OSError as e:
			print("your directory already exists!")
		self.send(self.getMacroValue("DOWNLOADFILE"))

		print("the files you can download are : \n")
		notFinished = False
		while not notFinished:
			message = self.receive()
			if (message!="FINISHED"):
				print(message+"\n")
			else:
				notFinished = True

		toDownload=input("type the file path you want to download:  ")
		self.send(toDownload)
		tmp=self.receive()
		if (tmp=="DIRECTORY"):
			print("le client veut telecharger initiallement un repertoire")
			self.receiveDirectory()
			print("apres tous les appels recursifs receiveDirectory")

		elif(tmp=="NOTDIRECTORY"):	
			print("le client veut telecharger initiallement un fichier")
			self.receiveFile("downloads",True)	
		input("press enter to go back ")


	def validateUsers(self):
		os.system("clear")
		print("Validate users menu\n")
		self.send(self.getMacroValue("VALIDATEUSERS"))
		notFinished = False
		while not notFinished:
			user = self.receive()
			if (user!="FINISHED"):
				print(user+"\n")
			else:
				notFinished = True
		toValidate=input("type the users name separated by a coma:  ")
		self.send(toValidate)

	def logOut(self):
		#Ferme la connection avec le serveur et on le renvoit au premier menu du login/sign up
		self.send(self.getMacroValue("LOGOUT"))

	def extractPKeyFromCertificate(self):
		with open(self._certificatePath, 'rb+') as f:
			cert_pem = f.read()
			f.close()
		x509 = crypto.load_certificate(crypto.FILETYPE_PEM, cert_pem)
		pub_key = x509.get_pubkey()
		public_key_string = crypto.dump_publickey(crypto.FILETYPE_PEM, pkey=pub_key).decode("utf-8")
		public_key = RSA.importKey(public_key_string)
		return public_key

	def encrypting(self,message):
		cipher = AES.new(key=self._aesKey, mode=AES.MODE_CTR, counter=Counter.new(AES.block_size * 8))
		return cipher.encrypt(message)

	def decrypting(self,message):
		cipher = AES.new(key=self._aesKey, mode=AES.MODE_CTR, counter=Counter.new(AES.block_size * 8))
		return cipher.decrypt(message)

	def send(self,data, file=False):
		self._ssocket.send(self.encrypting(data))

	def receive(self, file=False):
		if (not file):
			receivedData = self.decrypting(self._ssocket.recv(1024)).decode('ascii')
			receivedSign = self.decrypting(self._ssocket.recv(1024)).decode('ascii')
			if(self.verifySignature(receivedData,receivedSign)):
				return receivedData
			else:
				print("Message not verified!")
				self.quit()
				return None	
		else:
			receivedData = self.decrypting(self._ssocket.recv(16384))
			receivedSign = self.decrypting(self._ssocket.recv(16384))
			if(self.verifySignature(receivedData,receivedSign)):
				return receivedData
			else:
				print("Message not verified!")
				self.quit()
				return None
	
	def verifySignature(self,data,signature):
		try:
			crypto.verify(self._certificate,b64decode(signature),data,'sha256')
			return True
		except:
			return False

	def secureConnection(self):
		publicKey = self.extractPKeyFromCertificate()
		cipher = PKCS1_OAEP.new(publicKey)
		encryptedAESKey = cipher.encrypt(self._aesKey)
		self._ssocket.send(encryptedAESKey)
		
if __name__ == "__main__" :

	client = Client()
	client.close()
